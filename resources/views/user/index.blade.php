@include('includes.header_small')


<div class="description text-center">
    <p>User account page</p>
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="profile-tabs">
            <div class="nav-align-center">
                <ul class="nav nav-pills" role="tablist">

                    <li class="active">
                        <a href="#upcoming" role="tab" data-toggle="tab">
                            <i class="material-icons">palette</i>
                            Upcoming
                            {{-- @TODO: napraviti podjelu na 3 taba, all, events, todo --}}
                        </a>
                    </li>
                    <li>
                        <a href="#studio" role="tab" data-toggle="tab">
                            <i class="material-icons">camera</i>
                            Account
                        </a>
                    </li>

                    <li>
                        <a href="#password" role="tab" data-toggle="tab">
                            <i class="material-icons">lock_open</i>
                            Change Password
                        </a>
                    </li>
                </ul>
                {{-- end of list with icons --}}

                <div class="tab-content gallery">
                    <div class="tab-pane active text-center" id="upcoming">
                        <div class="row">
                            <a href="/todo" class="btn-block">Create new todo list</a>
                            <a href="/event" class="btn-block">Create new event</a>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div> Todos</div>
                                <table class="table table-striped">
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    @foreach(\App\Todo::all() as $todo)
                                        <tr>
                                            <td class=""><a href="/todo/{{$todo['id']}}/edit"> {{$todo['title']}}</a>
                                            </td>

                                            <td>{{$todo['description']}}</td>
                                            <td>
                                                @if(\App\Task::where('todo_id', $todo['id'])->where('completed', 'false')->first())
                                                    <i class="material-icons" style="color: red">clear</i>
                                                @else
                                                    <i class="material-icons" style="color: limegreen;">done_all</i>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/todo/{{$todo['id']}}/delete"> <i class="material-icons" style="color: #333333">delete_forever</i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>

                            <div class="col-sm-6">
                                <div> Events</div>
                                <table class="table table-striped">
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    @foreach(\App\Event::all() as $event)
                                        <tr>
                                            <td class=""><a href="/event/{{$event['id']}}/edit"> {{$event['title']}}</a>
                                            </td>

                                            <td>{{$event['description']}}</td>

                                            <td>
                                                {{ Form::open([
                                                    'method' => 'delete',
                                                    'route' => ['event.destroy', $event->id]
                                                ]) }}

                                                {{ Form::submit('delete_forever', ['class' => 'material-icons']) }}

                                                {{ Form::close() }}

                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- end of first icon tab --}}


                    {{-- tab for account info --}}
                    <div class="tab-pane" id="studio">
                        <div class="row">
                            {!! Form::open(array('action'=>'UserController@updateInfo','method'=>'post')) !!}
                            <div>
                                {!! Form::label('email', 'E-mail:', ['class'=>'test']) !!}
                                {!! Form::text('name', $value = Auth::User()->email , $attributes = ['disabled' => 'true', 'class' => 'form-control']) !!}
                                {!! Form::text('email', $value = Auth::User()->email , $attributes = ['hidden' => 'true']) !!}
                            </div>
                            <?php $test = Auth::user()->name ?>
                            <div>
                                {!! Form::label('name', 'Name:') !!}
                                {!!  Form::text('name', $test, $attributes = ['class' => 'form-control']) !!}
                            </div>
                            <br>
                            <div>
                                <div class="index-page" style="color: #AAAAAA;">Workdays:</div>
                                <table border="0" style="margin: auto" width="100%">
                                    <tr class="text-center">
                                        <td>{!! Form::label('monday', 'Monday') !!}</td>
                                        <td>{!! Form::label('tuesday', 'Tuesday') !!}</td>
                                        <td>{!! Form::label('wednesday', 'Wednesday') !!}</td>
                                        <td>{!! Form::label('thursday', 'Thursday') !!}</td>
                                        <td>{!! Form::label('friday', 'Friday') !!}</td>
                                        <td>{!! Form::label('saturday', 'Saturday') !!}</td>
                                        <td>{!! Form::label('sunday', 'Sunday') !!}</td>
                                    </tr>
                                    <tr class="text-center">
                                        @foreach($week[0] as $day)
                                            @if(in_array($day,$workdays))
                                                <td>{!! Form::checkbox('workday[]', $day, true, ['id' => $day]) !!}</td>
                                            @else
                                                <td>{!! Form::checkbox('workday[]', $day, null, ['id' => $day]) !!}</td>
                                            @endif
                                        @endforeach
                                    </tr>

                                </table>
                            </div>

                            <br>
                            <br>
                            <br>
                            <div>
                                Week starts with:
                                <br>
                                {!! Form::select('week_starts', [
                                'Monday' => 'Monday',
                                 'Tuesday' => 'Tuesday',
                                 'Wednesday' => 'Wednesday',
                                 'Thursday' => 'Thursday',
                                 'Friday' => 'Friday',
                                 'Saturday' => 'Saturday',
                                 'Sunday' => 'Sunday'
                                ], Auth::User()->week_starts ) !!}
                            </div>
                            {!! Form::submit('Save', $attributes = ['class'=>'button']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>


                    {{-- tab for password change --}}

                    <div class="tab-pane text-center" id="password">
                        <div class="row">
                            {!! Form::open(array('action'=>'UserController@changePassword','method'=>'post', 'class' => 'form')) !!}

                            {{-- current user info --}}
                            {!! Form::text('id', $value = Auth::User()->id , $attributes = ['hidden' => 'true']) !!}
                            {!! Form::text('email', $value = Auth::User()->email , $attributes = ['hidden' => 'true']) !!}

                            {{-- old password --}}
                            {!! Form::label('current_password', 'Current password') !!}
                            {!! Form::password('current_password', ['class' => 'form-control']) !!}

                            {{-- new password --}}
                            {!! Form::label('new_password', 'New password') !!}
                            {!! Form::password('new_password',  ['class' => 'form-control']) !!}

                            {{-- check new password --}}
                            {!! Form::label('retype_password', 'Retype password') !!}
                            {!! Form::password('retype_password',  ['class' => 'form-control']) !!}

                            {!! Form::submit('Change password', $attributes = ['class' => 'button']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Profile Tabs -->
    </div>
</div>

</div>
</div>
</div>

</div>

@include('includes.footer')
