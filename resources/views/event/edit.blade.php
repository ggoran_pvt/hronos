@include('includes.header_small')


<div class="tab-content gallery">



    <div class="tab-pane text-center active" id="task">
        <div class="row">
            {!! Form::open(array('action'=>['EventController@update', $event->id],'method'=>'put', 'class' => 'form')) !!}

            <div>
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', $event->title , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
            </div>

            <div>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::text('description', $event->description , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}
            </div>
            <div>
                {!! Form::label('event_starts', 'Event starts::') !!}
                {!! Form::datetime('event_starts', $event->event_starts, $attributes = ['class' => 'form-control']) !!}
            </div>
            <div>
                {!! Form::label('event_ends', 'Event ends:') !!}
                {!! Form::datetime('event_ends', $event->event_ends, $attributes = ['class' => 'form-control']) !!}
            </div>

            <div>
                {!! Form::text('user_id', $event->user_id , $attributes = ['hidden' => 'true', 'value'=> $event->user_id]) !!}
                {!! Form::text('id', $event->id , $attributes = ['hidden' => 'true', 'value'=> $event->id]) !!}
            </div>
            <div>
                {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@include('includes.footer')
