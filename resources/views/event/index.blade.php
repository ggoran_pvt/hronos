@include('includes.header_small')



<div class="tab-content gallery">



    <div class="tab-pane text-center active" id="task">
        <div class="row">
            {!! Form::open(array('action'=>'EventController@store','method'=>'post', 'class' => 'form')) !!}

            <div>
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
            </div>

            <div>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::text('description', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}
            </div>
            <div>
                {!! Form::label('event_start', 'Event starts::') !!}
                {!! Form::datetime('event_start', \Carbon\Carbon::now()->toDateTimeString(), $attributes = ['class' => 'form-control']) !!}
            </div>
            <div>
                {!! Form::label('event_end', 'Event ends:') !!}
                {!! Form::datetime('event_end', \Carbon\Carbon::now()->toDateTimeString(), $attributes = ['class' => 'form-control']) !!}
            </div>

            <div>
                {!! Form::text('user_id', '' , $attributes = ['hidden' => 'true', 'value'=> Auth::User()->id]) !!}
            </div>
            <div>
                {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@include('includes.footer')
