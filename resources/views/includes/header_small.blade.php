@include('includes.head')

<body class="profile-page">
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if(Auth::check())
                <a href="/user">
                    <div class="logo-container">
                        <div class="logo">
                            <img src="/assets/img/logo.png" rel="tooltip" title="<b>{{Auth::user()->name}}</b>" data-placement="bottom" data-html="true">
                        </div>
                        <div class="brand">
                            Hello, {{Auth::user()->name}}
                        </div>


                    </div>
                </a>
            @endif
        </div>

        <div class="collapse navbar-collapse" id="navigation-index">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/about">
                        <i class="material-icons">info</i> About
                    </a>
                </li>
                @if(!Auth::check())
                    <li>
                        <a href="/login" >
                            <i class="material-icons">face</i> Login
                        </a>
                    </li>

                    <li>
                        <a href="/register" >
                            <i class="material-icons">people</i> Register
                        </a>
                    </li>
                @elseif(Auth::check())
                    <li>
                        <a href="/logout" >
                            <i class="material-icons">power_settings_new</i> Logout
                        </a>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>
<div class="wrapper">
    <div class="header header-filter" style="background-image: url('/assets/img/examples/city.jpg')"></div>

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="profile">
                        <div class="avatar">
                            <img src="/assets/img/christian.jpg" alt="Circle Image"
                                 class="img-circle img-responsive img-raised">
                        </div>
                        <div class="name">
                            <h3 class="title">{{Auth::user()->name}}</h3>
                        </div>
                    </div>
                </div>