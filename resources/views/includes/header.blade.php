@include('includes.head')

<body class="index-page">
<!-- Navbar -->
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if(Auth::check())
                <a href="/user">
                    <div class="logo-container">
                        <div class="logo">
                            <img src="assets/img/logo.png" rel="tooltip" title="<b>{{Auth::user()->name}}</b>" data-placement="bottom" data-html="true">
                        </div>
                        <div class="brand">
                            Hello, {{Auth::user()->name}}
                        </div>


                    </div>
                </a>
            @endif
        </div>


        <div class="collapse navbar-collapse" id="navigation-index">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="/about">
                        <i class="material-icons">info</i> About
                    </a>
                </li>
                @if(!Auth::check())
                    <li>
                        <a href="/login" >
                        <i class="material-icons">face</i> Login
                        </a>
                    </li>

                    <li>
                        <a href="/register" >
                            <i class="material-icons">people</i> Register
                        </a>
                    </li>
                @elseif(Auth::check())
                    <li>
                        <a href="/logout" >
                            <i class="material-icons">power_settings_new</i> Logout
                        </a>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
