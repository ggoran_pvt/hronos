
<!-- Navbar -->
<nav class="navbar navbar-transparent navbar-fixed-top navbar-color-on-scroll">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if(Auth::check())
                <a href="/login">
                    <div class="logo-container">
                        <div class="logo">
                            <img src="assets/img/logo.png" rel="tooltip" title="<b>Login</b>" data-placement="bottom" data-html="true">
                        </div>
                        <div class="brand">
                            Login in hronos
                        </div>


                    </div>
                </a>
            @endif
        </div>


        <div class="collapse navbar-collapse" id="navigation-index">
            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::check())
                    <li>
                        <a href="/login" ">
                        <i class="material-icons">face</i> Login
                        </a>
                    </li>

                    <li>
                        <a href="/register" >
                            <i class="material-icons">people</i> Register
                        </a>
                    </li>
                @endif
                <li>
                    <a href="https://github.com/timcreative/material-kit/archive/master.zip" target="_blank">
                        <i class="material-icons">info</i> About
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
