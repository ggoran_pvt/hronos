@include('includes.header_small')

<h3>Edit Task</h3>
<div class="tab-content gallery">


    <div class="tab-pane text-center active" id="task">
        <div class="row">
            {!! Form::open(array(
            'action'=>'TaskController@editTask',
            'method'=>'post',
            'class' => 'form'
            )) !!}
            <div>
                {!! Form::label('todo_id', 'List:') !!}
                {!! Form::select('todo_id', \App\Todo::pluck('title', 'id'), $task->id, ['disabled'=>'true','value' => $task->todo_id]) !!}

            </div>
            <div>
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', $task->title , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
            </div>

            <div>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::text('description', $task->description , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}
            </div>
            <div>
                {!! Form::label('completed', 'Completed:') !!}
                @if($task->completed == 'false')
                    {!! Form::checkbox('completed' ) !!}
                    @else
                    {!! Form::checkbox('completed', $task->completed , true) !!}
                @endif
            </div>

            <div>
                {!! Form::text('user_id', '' , $attributes = ['hidden' => 'true', 'value'=> Auth::User()->id]) !!}
                {!! Form::text('id', $task->id , $attributes = ['hidden' => 'true', 'value'=> $task->id]) !!}
            </div>

            <div>
                {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<a href="{{ URL::previous() }}">Go Back</a>

@include('includes.footer')
