
<div class="wrapper">
    <div class="header header-filter" style="background-image: url('assets/img/bg2.jpeg');">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand txt-white">
                        <h1>Hronos</h1>
                        <h3>A Badass time manager</h3>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="main main-raised">
        <div class="section section-basic">
            <div class="container">
                @yield('container')
            </div>
        </div>

    </div>


@include('includes.footer')
