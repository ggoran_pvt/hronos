@include('includes.header_small')

<ul class="nav nav-pills" role="tablist">

    <li class="active">
        <a href="#todo" role="tab" data-toggle="tab">
            <i class="material-icons">palette</i>
            List
        </a>
    </li>
    <li>
        <a href="#task" role="tab" data-toggle="tab">
            <i class="material-icons">camera</i>
            Tasks
        </a>
    </li>
</ul>
{{-- end of list with icons --}}

<div class="tab-content gallery">

    <div class="tab-pane active text-center" id="todo">
        <div class="row">
            {!! Form::open(array('action'=>'TodoController@create','method'=>'post', 'class' => 'form')) !!}
            <div>
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
            </div>

            <div>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::text('description', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}

            </div>
            {{--
            repeat disabled
                        <div>
                            {!! Form::label('repeat_id', 'Repeats:') !!} <br>
                            {!! Form::select('repeat_id', [  'Monday'   => 'Monday',
                                                             'Tuesday'  => 'Tuesday',
                                                             'Wednesday'=> 'Wednesday',
                                                             'Thursday' => 'Thursday',
                                                             'Friday'   => 'Friday',
                                                             'Saturday' => 'Saturday',
                                                             'Sunday'   => 'Sunday'
                                                            ] ) !!}
                        </div>
            --}}
            <div>
                {!! Form::text('user_id', '' , $attributes = ['hidden' => 'true', 'value'=> Auth::User()->id]) !!}
            </div>
            <div>
                {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
            </div>
            {!! Form::close() !!}


        </div>
    </div>

    <div class="tab-pane text-center" id="task">
        <div class="row">
            {!! Form::open(array('action'=>'TaskController@create','method'=>'post', 'class' => 'form')) !!}
            <div>
                {!! Form::label('todo_id', 'List:') !!}
                {!! Form::select('todo_id', \App\Todo::pluck('title', 'id'), null, ['placeholder' => 'Select list...']) !!}

            </div>
            <div>
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
            </div>

            <div>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::text('description', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}
            </div>

            <div>
                {!! Form::text('user_id', '' , $attributes = ['hidden' => 'true', 'value'=> Auth::User()->id]) !!}
            </div>
            <div>
                {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@include('includes.footer')
