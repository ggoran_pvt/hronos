@include('includes.header_small')

<h3>Edit Todo List</h3>
    <div class="tab-pane active text-center" id="todo">
        <div class="row">
            {!! Form::open(array('action'=>'TodoController@editTodo','method'=>'post', 'class' => 'form')) !!}
            <div>
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', $todo->title , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
            </div>

            <div>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::text('description', $todo->description , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}

            </div>
            {{--
            repeat disabled
                        <div>
                            {!! Form::label('repeat_id', 'Repeats:') !!} <br>
                            {!! Form::select('repeat_id', [  'Monday'   => 'Monday',
                                                             'Tuesday'  => 'Tuesday',
                                                             'Wednesday'=> 'Wednesday',
                                                             'Thursday' => 'Thursday',
                                                             'Friday'   => 'Friday',
                                                             'Saturday' => 'Saturday',
                                                             'Sunday'   => 'Sunday'
                                                            ] ) !!}
                        </div>
            --}}
            <div>
                {!! Form::text('user_id', '' , $attributes = ['hidden' => 'true', 'value'=> Auth::User()->id]) !!}
                {!! Form::text('id', $todo->id , $attributes = ['hidden' => 'true', 'value'=> $todo->id]) !!}
            </div>
            <div>
                {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
            </div>
            {!! Form::close() !!}


        </div>
    </div>
<h3>Add new task</h3>
<div class="tab-pane text-center" id="task">
    <div class="row">
        {!! Form::open(array('action'=>'TaskController@create','method'=>'post', 'class' => 'form')) !!}
        <div>
            {!! Form::select('todo_id', \App\Todo::pluck('title', 'id'), $todo->id, ['hidden'=>'true', 'placeholder' => 'Select list...', 'value'=>$todo->id]) !!}

        </div>
        <div>
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Title']) !!}
        </div>

        <div>
            {!! Form::label('description', 'Description:') !!}
            {!! Form::text('description', '' , $attributes = ['class' => 'form-control', 'placeholder'=>'Description']) !!}
        </div>

        <div>
            {!! Form::text('user_id', '' , $attributes = ['hidden' => 'true', 'value'=> Auth::User()->id]) !!}
        </div>
        <div>
            {!! Form::submit('Save', $attributes = ['class' => 'btn']) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>

<table class="table table-striped">
    <tr>
        <th>Title</th>
        <th>Description</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>

    @foreach($tasks as $task)
        <tr>
            <td class=""><a href="/task/{{$task['id']}}/edit"> {{$task['title']}}</a></td>
            <td>{{$task['description']}}</td>
            <td>
                @if($task['completed'] == 'false')
                    <i class="material-icons">clear</i>
                @else
                    <i class="material-icons">done_all</i>
                @endif
            </td>
            <td><a href="/task/{{$task['id']}}/delete" style="color: #333333"> <i class="material-icons">delete</i></a></td>
        </tr>
    @endforeach

</table>

@include('includes.footer')
