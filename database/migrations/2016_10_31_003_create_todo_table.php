<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('repeat_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->timestamps();
        });

        Schema::table('todo', function(Blueprint $table)
        {
            //$table->foreign('repeat_id')->references('id')->on('repeat');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('todo');
    }
}
