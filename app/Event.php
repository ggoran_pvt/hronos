<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

    protected $table = 'event';

    protected $fillable = ['title', 'description', 'user_id', 'event_starts', 'event_end'];
}
