<?php

namespace App\Http\Controllers;

use App\Task;
use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class TodoController extends Controller
{
    //
    public function index() {
        // get all todos
        $todos = DB::table('todo')->get();

        return view('/todo/index', compact('todos'));
    }

    public function create() {
        //
        Todo::create(array(
            'title'=>Input::get('title'),
            'description'=>Input::get('description'),
            'user_id' => Auth::user()->id
        ));

        return Redirect::route('/todo/index');
    }

    public function editTodo(Request $request)
    {
        $todo = Todo::where('user_id', $request->id)->first();
        if (Auth::user() != $todo->user_id){
            return redirect()->back();
        }

        Todo::where('id', $request->id)
            ->update([  'title'=>$request->title,
                        'description'=>$request->description
            ]);
        return redirect()->back();
    }

    public function getTasks($id) {
        return Task::where('todo_id', $id)->get();
    }

    public function editView($id)
    {
        $todo = Todo::where('user_id', $id)->first();
        if (Auth::user() != $todo->user_id){
            return redirect()->back();
        }

        $this->tasks = $this->getTasks($id);
        return View::make('todo.edit')
            ->with('todo', Todo::find($id))
            ->with('tasks', $this->tasks);
    }


    public function delete($id)
    {
        $todo = Todo::where('user_id', $id)->first();
        if (Auth::user() != $todo->user_id){
            return redirect()->back();
        }

        Todo::destroy($id);
        return redirect()->back();
    }
}
