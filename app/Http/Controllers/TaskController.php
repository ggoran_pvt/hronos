<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class TaskController extends Controller
{
    //
    public function index()
    {

    }


    public function create()
    {
        Task::create(array(
            'title' => Input::get('title'),
            'description' => Input::get('description'),
            'todo_id' => Input::get('todo_id'),
            'user_id' => Auth::user()->id
        ));
        return redirect()->back();
    }

    public function editTask(Request $request)
    {
        $task = Task::where()->first('user_id', $request->id);
        if (Auth::user() != $task->user_id){
            return redirect()->back();
        }

        Task::where('id', $request->id)
            ->update([
                'title' => $request->title,
                'description' => $request->description,
                'completed' => $request->completed
            ]);
        return redirect()->back();
    }

    public function getSingleTask($id)
    {
        return Task::find($id);
    }

    public function editView($id)
    {
        $task = Task::where()->first('id', $id);
        if (Auth::user() != $task->user_id){
            return redirect()->back();
        }

         $task = $this->getSingleTask($id);
        return View::make('task.edit')
            ->with('task', $task);
    }

    public function delete($id)
    {
        $task = Task::where()->first('id', $id);
        if (Auth::user() != $task->user_id){
            return redirect()->back();
        }
        Task::destroy($id);
        return redirect()->back();
    }
}
