<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Redirect;
use Auth;

class UserController extends Controller
{
    //
    public function index() {
        // get info from DB
        $userWorkdays = json_decode(Auth::User()->workdays);
        $week[] = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");

        return view('user/index')->with(array('workdays'=>$userWorkdays, 'week'=>$week));
    }

    public function updateInfo(Request $request) {
        // get request input
        $email = $request->input('email');
        // json encode - array to string conversation use decode when reading from DB
        $workdays = json_encode($request->input('workday'));
        $week_starts = $request->input('week_starts');
        $name = $request->input('name');

        // update DB
        DB::table('users')
            ->where('email', $email)
            ->update(['name'=>$name,'workdays'=>$workdays, 'week_starts'=> $week_starts]);

        // redirect to admin page
        return Redirect::route('user');
    }


    public function changePassword(Request $request) {
//        dd(Input::get());

        $currentPassword = Input::get('current_password');
        $newPassword = Input::get('new_password');
        $newRetype = Input::get('retype_password');
        $id = Auth::user()->id;
        $passwordHash = Hash::make($newPassword);

        if(Hash::check($passwordHash, Auth::user()->password) == false
            && $currentPassword != $newPassword
            && $newPassword == $newRetype
            && $newPassword != ''
            && $newPassword != null){

            // change pass
            // update table
            DB::table('users')
                ->where('id', $id)
                ->update(['password'=>$passwordHash]);

            return Redirect::route('user');

        }
        else {
            // error
            echo 'error <br>';
        }
    }

}
