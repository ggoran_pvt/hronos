<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $events = DB::table('event')->get();

        return view('/event/index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request->all());

        // validation
        $this->validate($request,[
            'title'=> 'required',
            'description' => 'required',
            'event_start' => 'required',
            'event_end' => 'required',
        ]);

        // process
        $event = new Event();

            $event->title = $request->title;
            $event->description = $request->description;
            $event->event_starts = $request->event_start;
            $event->event_ends = $request->event_end;
            $event->user_id = Auth::user()->id;

        $event->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::where('id', $id)->first();
        if(Auth::user() != $event->user_id){
            return redirect()->back();
        }
        // get event
        $event = Event::findOrFail($id);

        return View::make('event.edit')->with('event', $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $event = Event::where('id', $id)->first();
        if(Auth::user() != $event->user_id){
            return redirect()->back();
        }
        $event = Event::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required'
        ]);

        $event->update($request->all());

        return Redirect::route('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $event = Event::where('id', $id)->first();
        if(Auth::user() != $event->user_id){
            return redirect()->back();
        }
        Event::destroy($id);
        return Redirect::route('user');
    }
}
