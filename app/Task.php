<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $table = 'task';

    protected $fillable = ['title', 'description', 'user_id', 'todo_id', 'completed'];
}
