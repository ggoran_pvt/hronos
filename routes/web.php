<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about', function () {
    return view('about');
});

Route::get('/user', 'UserController@index')->name('user')->middleware('auth');
Route::post('/user/update', 'UserController@updateInfo')->middleware('auth');
Route::post('/user/change_password', 'UserController@changePassword')->name('/user/change_password')->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/todo', 'TodoController@index')->name('/todo/index')->middleware('auth');
Route::get('/todo', 'TodoController@index')->name('/todo/index')->middleware('auth');
Route::get('/todo/{id}/edit', 'TodoController@editView')->middleware('auth');
Route::get('/todo/{id}/delete', 'TodoController@delete')->middleware('auth');

Route::post('/todo/editTodo', 'TodoController@editTodo')->middleware('auth');
Route::post('/todo', 'TodoController@create')->middleware('auth');



Route::post('/task', 'TaskController@create')->middleware('auth');
Route::get('/task/{id}/edit', 'TaskController@editView')->middleware('auth');
Route::post('/todo/editTask', 'TaskController@editTask')->middleware('auth');
Route::get('/task/{id}/delete', 'TaskController@delete')->middleware('auth');

Route::get('/logout', 'Auth\LoginController@logout');

Route::resource('/event', 'EventController');